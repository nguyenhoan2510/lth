﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HtmlAgilityPack;
using System.Xml.Linq;
using System.IO;
using Facebook;
using System.Web.Routing;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using MVCScopedClients.Models;
using System.Text;

namespace FakeShareImageClient.Controllers
{
    public class HomeController : Controller
    {
        //Trang chủ
        public ActionResult Index()
        {
            return View();
        }

        //Hình của user
        [Authorize]
        public ActionResult ImageUser()
        {
            return View();
        }
		
		//Chi tiết hình ảnh
        public ActionResult ImageDetail(int id)
        {
            ViewBag.id = id.ToString();
            TempData["id"] = id.ToString();
            return View();
        }
		
		//Tải ảnh lên website
        [Authorize]
        public ActionResult uploadImage()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult uploadImage(string hinhanh, string tieude, string nguoidang)
        {
            //string path = "";
            //string delpath = "";

            //foreach (string file in Request.Files)
            //{
            //    var hpf = this.Request.Files[file];
            //    if (hpf.ContentLength == 0)
            //    {
            //        continue;
            //    }

            //    string savedFileName = Path.Combine(
            //        AppDomain.CurrentDomain.BaseDirectory, "upload");
            //    savedFileName = Path.Combine(savedFileName, Path.GetFileName(hpf.FileName));

            //    //Lấy đường dẫn full của website
            //    path = savedFileName;
            //    delpath = savedFileName;
            //    //Lấy tên file
            //    hinhanh = Path.GetFileName(hpf.FileName);

            //    hpf.SaveAs(savedFileName);

                
            //}
            //byte[] transf = GetFileByteArray(path);
            //UTF8Encoding enc = new UTF8Encoding();
            //string x = enc.GetString(transf);
            
            ////if (System.IO.File.Exists(path))
            ////{
            ////    System.IO.File.Delete(path);
            ////}

            //tieude = Request["tags"];
            //nguoidang = User.Identity.Name;

            ////xử lý chuỗi có khoảng trắng
            //hinhanh = hinhanh.Replace(" ", "%20");
            //tieude = tieude.Replace(" ", "%20");
            //nguoidang = nguoidang.Replace(" ", "%20");
            //path = path.Replace(" ", "%20");
            ////xử lý đường dẫn
            //path = path.Replace("\\", "@");
            //delpath = delpath.Replace("\\", "\\\\");

            //string chuoi = "";
            //chuoi += "var json = '" + delpath + "';";
            //chuoi += "var url = '/home/deleteimage';";
            //chuoi += "$.post(url, { path: path }, function (data) {alert('Tải ảnh thành công.');});";
            //string query = "$.post(\"http://localhost:3297/api/Photo?hinhAnh=" + hinhanh + "&tieuDe=" + tieude + "&nguoiDang=" + nguoidang + "&url=" + path + "\",";
            //query += "function (data) {" + chuoi + "}).error(function () { alert(\"Đã có lỗi xảy ra trong quá trình tải ảnh.\"); });";
            //ViewBag.delpath = delpath;
            //ViewBag.path = path;
            ////xoa anh


            //ViewBag.StartupScript = query;
            //ViewBag.DeleteImage = chuoi;

            

            //var message = new HttpRequestMessage();
            //var content = new MultipartFormDataContent();


            //var filestream = new FileStream("C:\\fakepath\\logo-1.png", FileMode.Open);
            //    var fileName = "logo-1.png";
            //    content.Add(new StreamContent(filestream), "file", fileName);
            

            //message.Method = System.Net.Http.HttpMethod.Post;
            //message.Content = content;
            //message.RequestUri = new Uri("http://localhost:3297/api/Values");

            //var client = new HttpClient();
            //client.SendAsync(message).ContinueWith(task =>
            //{
            //    if (task.Result.IsSuccessStatusCode)
            //    {
            //        //do something with response
            //    }
            //});    
            
            return View();
        }

        public ActionResult uploadsuccess()
        {
            return View();
        }

		//Quản lý ảnh
		public ActionResult quanlyanh()
		{
			return View();
		}

		[HttpPost]
		public ActionResult deleteimage(string path)
		{
			if (System.IO.File.Exists(path))
			{
				System.IO.File.Delete(path);
			}
			return View("index");
		}
    }
}
