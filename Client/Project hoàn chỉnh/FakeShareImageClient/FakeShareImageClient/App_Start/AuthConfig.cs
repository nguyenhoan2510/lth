﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using FakeShareImageClient.Models;


namespace FakeShareImageClient
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");

            Dictionary<string, object> FacebooksocialData = new Dictionary<string, object>() ;
            FacebooksocialData.Add("scope", "publish_stream, user_photos");
            FacebooksocialData.Add("Icon", "~/Content/images/fb.png");
            
            OAuthWebSecurity.RegisterFacebookClient(

                appId: "628030583900514",
                appSecret: "1c2b337bf93c889545e18d6e655b4f85",
                displayName: "Facebook",
                extraData: FacebooksocialData);

            //OAuthWebSecurity.RegisterClient(new FacebookScopedClient("628030583900514", "1c2b337bf93c889545e18d6e655b4f85", "user_photos"),"Facebook", new Dictionary<string, object>());

            OAuthWebSecurity.RegisterGoogleClient();

            OAuthWebSecurity.RegisterYahooClient();
        }
    }
}
