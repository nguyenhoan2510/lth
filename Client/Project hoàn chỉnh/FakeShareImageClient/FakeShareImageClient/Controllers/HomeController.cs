﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HtmlAgilityPack;
using System.Xml.Linq;
using System.IO;
using Facebook;
using System.Web.Routing;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using MVCScopedClients.Models;
using System.Text;

namespace FakeShareImageClient.Controllers
{
    public class HomeController : Controller
    {
        //Trang chủ
        public ActionResult Index()
        {
            return View();
        }

        //Hình của user
        [Authorize]
        public ActionResult ImageUser()
        {
            return View();
        }

        //hình ảnh từ flickr
        public ActionResult flickr()
        {
            HtmlDocument doc = new HtmlDocument();
            HtmlWeb hw = new HtmlWeb();
            doc = hw.Load("http://www.flickr.com/explore");
            //<img id="photo_img_12482797915" data-defer-src="https://ycpi-farm3.staticflickr.com/2851/12482797915_5e0270834b_z.jpg" src="http://l.yimg.com/g/images/spaceball.gif" width="377" height="242" alt="Notre Dame Cathedral Basilica of Ottawa" class="pc_img defer" border="0">

            HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(".//*[@class='photo_container pc_ju']/a/img/@data-defer-src");

            string chuoi = "";
            int i = 0;
            int j = 0;

            foreach (var item in nodes)
            {
                if (i % 2 == 0)
                {
                    chuoi += "<div class=\"row\" style=\"width: 1331px\">";
                }

                chuoi += "<div class=\"photo-display-item\" id=\'" + item.Id + "'\" style=\"z-index: inherit;\">";
                chuoi += "<div class=\"hover-target\">";
                chuoi += "<div class=\"thumb \">";
                chuoi += "<span class=\"photo_container pc_ju\">";
                chuoi += "<a data-track=\"photo-click\" data-lightbox=\'" + item.Id + "'\" href=\'" + item.Attributes["data-defer-src"].Value + "'\" class=\"rapidnofollow photo-click\" alt=\'" + item.Attributes["alt"].Value + "'\">";
                chuoi += replaceString(item.OuterHtml);
                chuoi += "<div class=\"play\"></div>";
                chuoi += "</a>";
                chuoi += "</span>";
                chuoi += "<div class=\"meta\">";
                chuoi += "<div class=\"title\" style=\"display: none;\">";
                chuoi += "<a data-track=\"photo-click\" href=\"\" title=\'" + item.Attributes["alt"].Value + "'\" class=\"title\">" + item.Attributes["alt"].Value + "</a>";
                chuoi += "</div>";
                chuoi += "<div class=\"attribution-block\">";
                chuoi += "<span class=\"attribution\">";
                chuoi += "<span>từ </span>";
                chuoi += "<a data-track=\"owner\" href=\"http://flickr.com/explore\" title=\"Flickr\" class=\"owner\">flickr</a>";
                chuoi += "</span>";
                chuoi += "</div>";
                chuoi += "</div>";
                chuoi += "</div>";
                chuoi += "</div>";
                chuoi += "</div>";

                if ((j = i + 1) % 2 == 0)
                {
                    chuoi += "</div>";
                }

                i++;
            }

            ViewBag.image = chuoi;

            return View();
        }

        //Xử lý chuỗi lấy từ flickr
        public string replaceString(string chuoi)
        {
            string x = chuoi.Replace("src=\"http://l.yimg.com/g/images/spaceball.gif\"", "style=\"height:297px\"");
            x = x.Replace("data-defer-src", "src");
            x = x.Replace("width", "old-width");
            return x;
        }

        //Chi tiết hình ảnh
        public ActionResult ImageDetail(int id)
        {
            ViewBag.id = id.ToString();
            TempData["id"] = id.ToString();
            return View();
        }

        //Share ảnh lên Facebook
        [HttpPost]
        public ActionResult postFacebook(string hinhAnh, string noiDung)
        {
            hinhAnh = Request["txtUrl"];
            noiDung = Request["txtNoiDung"];

            HttpCookie c = Request.Cookies["token"];
            if (c != null)
            {
                var client = new FacebookClient(System.Web.HttpContext.Current.Request.Cookies["token"].Value);

                var postparameters = new Dictionary<string, object>();

                var media = new FacebookMediaObject
                {
                    FileName = hinhAnh,
                    ContentType = "image/jpeg"
                };

                var webClient = new WebClient();
                byte[] img = webClient.DownloadData(hinhAnh);
                media.SetValue(img);
                postparameters["name"] = noiDung;
                postparameters["source"] = media;
                postparameters["access_token"] = System.Web.HttpContext.Current.Request.Cookies["token"].Value;
                var result = client.Post("/me/photos", postparameters);
            }

            ///418154001582755/photos
            return View();
        }


        //Tải ảnh lên website
        [Authorize]
        public ActionResult uploadImage()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult uploadImage(string hinhanh, string tieude, string nguoidang)
        {
            //string path = "";
            //string delpath = "";

            //foreach (string file in Request.Files)
            //{
            //    var hpf = this.Request.Files[file];
            //    if (hpf.ContentLength == 0)
            //    {
            //        continue;
            //    }

            //    string savedFileName = Path.Combine(
            //        AppDomain.CurrentDomain.BaseDirectory, "upload");
            //    savedFileName = Path.Combine(savedFileName, Path.GetFileName(hpf.FileName));

            //    //Lấy đường dẫn full của website
            //    path = savedFileName;
            //    delpath = savedFileName;
            //    //Lấy tên file
            //    hinhanh = Path.GetFileName(hpf.FileName);

            //    hpf.SaveAs(savedFileName);

                
            //}
            //byte[] transf = GetFileByteArray(path);
            //UTF8Encoding enc = new UTF8Encoding();
            //string x = enc.GetString(transf);
            
            ////if (System.IO.File.Exists(path))
            ////{
            ////    System.IO.File.Delete(path);
            ////}

            //tieude = Request["tags"];
            //nguoidang = User.Identity.Name;

            ////xử lý chuỗi có khoảng trắng
            //hinhanh = hinhanh.Replace(" ", "%20");
            //tieude = tieude.Replace(" ", "%20");
            //nguoidang = nguoidang.Replace(" ", "%20");
            //path = path.Replace(" ", "%20");
            ////xử lý đường dẫn
            //path = path.Replace("\\", "@");
            //delpath = delpath.Replace("\\", "\\\\");

            //string chuoi = "";
            //chuoi += "var json = '" + delpath + "';";
            //chuoi += "var url = '/home/deleteimage';";
            //chuoi += "$.post(url, { path: path }, function (data) {alert('Tải ảnh thành công.');});";
            //string query = "$.post(\"http://localhost:3297/api/Photo?hinhAnh=" + hinhanh + "&tieuDe=" + tieude + "&nguoiDang=" + nguoidang + "&url=" + path + "\",";
            //query += "function (data) {" + chuoi + "}).error(function () { alert(\"Đã có lỗi xảy ra trong quá trình tải ảnh.\"); });";
            //ViewBag.delpath = delpath;
            //ViewBag.path = path;
            ////xoa anh


            //ViewBag.StartupScript = query;
            //ViewBag.DeleteImage = chuoi;

            

            //var message = new HttpRequestMessage();
            //var content = new MultipartFormDataContent();


            //var filestream = new FileStream("C:\\fakepath\\logo-1.png", FileMode.Open);
            //    var fileName = "logo-1.png";
            //    content.Add(new StreamContent(filestream), "file", fileName);
            

            //message.Method = System.Net.Http.HttpMethod.Post;
            //message.Content = content;
            //message.RequestUri = new Uri("http://localhost:3297/api/Values");

            //var client = new HttpClient();
            //client.SendAsync(message).ContinueWith(task =>
            //{
            //    if (task.Result.IsSuccessStatusCode)
            //    {
            //        //do something with response
            //    }
            //});    
            
            return View();
        }

        public ActionResult uploadsuccess()
        {
            return View();
        }

        [HttpPost]
        public ActionResult deleteimage(string path)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            return View("index");
        }
        //Convert to byte[]
        private byte[] GetFileByteArray(string filename)
        {
            FileStream oFileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file size.
            byte[] FileByteArrayData = new byte[oFileStream.Length];

            //Read file in bytes from stream into the byte array
            oFileStream.Read(FileByteArrayData, 0, System.Convert.ToInt32(oFileStream.Length));

            //Close the File Stream
            oFileStream.Close();

            return FileByteArrayData; //return the byte data
        }

        //Trình diễn ảnh
        public ActionResult trinhdienanh()
        {
            //http://www.fakeapi.somee.com
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync("http://localhost:3297/api/photo?user=" + User.Identity.Name).Result;
            var x = response.Content.ReadAsAsync<IEnumerable<ImageModel>>().Result;
            string chuoi = "<tiltviewergallery><photos>";
            foreach (var item in x)
            {
                chuoi += "<photo imageurl=\"http://localhost:3297/upload/" + item.HinhAnh + "\">";
                chuoi += "<title>" + item.TieuDe + "</title>";
                chuoi += "<description>bởi " + item.NguoiDang + ".</description>";
                chuoi += "</photo>";
            }
            chuoi += "</photos></tiltviewergallery>";

            string savedFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "upload");
            savedFileName = Path.Combine(savedFileName, User.Identity.Name + ".xml");
            FileInfo fi = new FileInfo(savedFileName);

            if (!fi.Exists)
            {
                using (StreamWriter writer = fi.CreateText())
                {
                    writer.WriteLine(chuoi);
                }
            }
            else
            {
                System.IO.File.WriteAllText(savedFileName, chuoi);
            }
            return View();
        }

        //Quản lý ảnh
        public ActionResult quanlyanh()
        {
            return View();
        }

        //flickr slide
        public ActionResult flickrslide()
        { return View(); }
    }
}
