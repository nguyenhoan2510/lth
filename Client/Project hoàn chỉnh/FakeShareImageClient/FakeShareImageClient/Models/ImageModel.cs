﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCScopedClients.Models
{
    public class ImageModel
    {
        public int Id { get; set; }
        public string HinhAnh { get; set; }
        public string TieuDe { get; set; }
        public int LuotLike { get; set; }
        public string NguoiDang { get; set; }
    }
}