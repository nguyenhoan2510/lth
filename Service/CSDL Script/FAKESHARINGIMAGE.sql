USE [FAKESHARINGIMAGE]
GO
/****** Object:  Table [dbo].[Image]    Script Date: 2/26/2014 1:47:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Image](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HinhAnh] [nvarchar](50) NULL,
	[TieuDe] [nvarchar](50) NULL,
	[LuotLike] [int] NULL,
	[NguoiDang] [nvarchar](50) NULL,
 CONSTRAINT [PK_Image] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Image] ON 

INSERT [dbo].[Image] ([Id], [HinhAnh], [TieuDe], [LuotLike], [NguoiDang]) VALUES (1, N'cat.jpg', N'Black Cat', 10, N'admin')
INSERT [dbo].[Image] ([Id], [HinhAnh], [TieuDe], [LuotLike], [NguoiDang]) VALUES (2, N'cat.jpg', N'Black Cat', 10, N'admin')
INSERT [dbo].[Image] ([Id], [HinhAnh], [TieuDe], [LuotLike], [NguoiDang]) VALUES (3, N'cat.jpg', N'Black Cat', 10, N'admin')
INSERT [dbo].[Image] ([Id], [HinhAnh], [TieuDe], [LuotLike], [NguoiDang]) VALUES (4, N'cat.jpg', N'Black Cat', 10, N'kai.251091@yahoo.com.vn')
INSERT [dbo].[Image] ([Id], [HinhAnh], [TieuDe], [LuotLike], [NguoiDang]) VALUES (5, N'cat.jpg', N'Black Cat', 10, N'kai.251091@yahoo.com.vn')
INSERT [dbo].[Image] ([Id], [HinhAnh], [TieuDe], [LuotLike], [NguoiDang]) VALUES (6, N'cat.jpg', N'Black Cat', 10, N'kai.251091@yahoo.com.vn')
SET IDENTITY_INSERT [dbo].[Image] OFF
