﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SinhVienService.Models;
using System.IO;

namespace SinhVienService.Controllers
{
    public class PhotoController : ApiController
    {
        FAKESHARINGIMAGEEntities db = new FAKESHARINGIMAGEEntities();

        // GET api/photo
        public IEnumerable<Image> Get()
        {
            var img = db.Images.OrderBy(x => x.Id).ToList();
            return img;
        }

        // GET api/photo/{user}
        public IEnumerable<Image> GetRiaUser(string user)
        {
            var img = db.Images.Where(x => x.NguoiDang.Equals(user)).OrderByDescending(x => x.Id).ToList();
            return img;
        }

        // GET api/photo/id
        public Image GetById(int id)
        {
            var img = db.Images.First(x=>x.Id==id);
            return img;
        }

        // POST api/photo
        public void Post(string hinhAnh, string tieuDe,string nguoiDang, string url)
        {
            Image img = new Image();
            img.HinhAnh = hinhAnh;
            img.TieuDe = tieuDe;
            img.LuotLike = 0;
            img.NguoiDang = nguoiDang;

            db.Images.Add(img);
            db.SaveChanges();

            string local = Path.Combine(
                    AppDomain.CurrentDomain.BaseDirectory, "upload");
            local = Path.Combine(local, hinhAnh);

            url = url.Replace("@", "/");

            WebClient wc = new WebClient();
            wc.DownloadFile(url, local);
        }

		// PUT api/photo/5
		public void Put(int id, string tieude)
		{
			Image img = db.Images.First(x => x.Id == id);
			img.TieuDe = tieude;
			db.SaveChanges();
		}

		// DELETE api/photo/5
		public void Delete(int id)
		{
			Image img = db.Images.First(x => x.Id == id);

			//lay duong dan toi hinh anh
			string path = Path.Combine(
				AppDomain.CurrentDomain.BaseDirectory, "upload");
			path = Path.Combine(path, img.HinhAnh);

			//Xoa du lieu trong csdl
			db.Images.Remove(img);
			db.SaveChanges();

			//xoa hinh anh
			if (System.IO.File.Exists(path))
			{
				System.IO.File.Delete(path);
			}
		}
    }
}
